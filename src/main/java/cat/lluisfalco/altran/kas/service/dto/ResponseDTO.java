package cat.lluisfalco.altran.kas.service.dto;

public class ResponseDTO {
	
	public ResponseDTO() {

	}
	
	public ResponseDTO(String code,String orgDescription, String url) {
		setCode(code);
		setOrgDescription(orgDescription);
		setUrl(url);
	}
			
	String code;

	String orgDescription;
		
	String url;
		

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getOrgDescription() {
		return orgDescription;
	}

	public void setOrgDescription(String orgDescription) {
		this.orgDescription = orgDescription;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString(){
		StringBuffer sb = new StringBuffer();
	       sb
	       .append("{")
	       .append("Code: ").append(getCode())
	       .append(", Organitzation description: ").append(getOrgDescription())
	       .append(", Url: ").append(getUrl())
	       .append("}");
		    return sb.toString();
	}


}
