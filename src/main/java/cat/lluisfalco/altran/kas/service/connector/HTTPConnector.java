package cat.lluisfalco.altran.kas.service.connector;

import java.io.Reader;

import org.springframework.stereotype.Service;

@Service
public interface HTTPConnector {

	Reader performSimpleRequest() throws Exception;

}
