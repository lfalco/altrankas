package cat.lluisfalco.altran.kas.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import cat.lluisfalco.altran.kas.service.dto.ResponseDTO;


@Service
public interface SearchService {

	ArrayList<ResponseDTO> getAllCodeAndDescriptionAndUrl(String language);

}
