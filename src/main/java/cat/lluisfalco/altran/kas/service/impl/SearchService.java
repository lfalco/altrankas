package cat.lluisfalco.altran.kas.service.impl;

import java.io.Reader;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import cat.lluisfalco.altran.kas.model.Result;
import cat.lluisfalco.altran.kas.model.SearchResult;
import cat.lluisfalco.altran.kas.service.connector.HTTPConnector;
import cat.lluisfalco.altran.kas.service.dto.ResponseDTO;


@Service
public class SearchService implements cat.lluisfalco.altran.kas.service.SearchService {

	@Autowired
	HTTPConnector httpConnector;
	
	@Override
	public ArrayList<ResponseDTO> getAllCodeAndDescriptionAndUrl(String language) {
		try {			
			Reader searchResultReader = httpConnector.performSimpleRequest();
			
			Gson gson = new Gson();
			SearchResult searchResult = gson.fromJson(searchResultReader, SearchResult.class);

			ArrayList<ResponseDTO> responseList = new ArrayList<ResponseDTO>();
			
			for(Result result : searchResult.getResult().getResults()) {
				/**Feo*/
				switch(language) {
					case "ca":
						responseList.add(new ResponseDTO(result.getCode(), 
								result.getOrganization().getDescription_translated().getCa(), result.getUrl_tornada().getCa()));
						break;
					case "es":
						responseList.add(new ResponseDTO(result.getCode(),
								result.getOrganization().getDescription_translated().getEs(), result.getUrl_tornada().getEs()));
						break;
					case "en":
						responseList.add(new ResponseDTO(result.getCode(),
								result.getOrganization().getDescription_translated().getEn(), result.getUrl_tornada().getEn()));
						break;
				}
			}
			
			return responseList;
		
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}	
	}

}
