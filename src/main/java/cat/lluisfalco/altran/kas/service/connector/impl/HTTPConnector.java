package cat.lluisfalco.altran.kas.service.connector.impl;

import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

import org.springframework.stereotype.Service;


@Service
public class HTTPConnector implements cat.lluisfalco.altran.kas.service.connector.HTTPConnector {

	
	
	@Override
	public Reader performSimpleRequest() throws Exception {
		try {	
			URL obj = new URL("http://opendata-ajuntament.barcelona.cat/data/api/3/action/package_search");
			HttpURLConnection con =  (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			         
			Reader reader = new InputStreamReader(con.getInputStream(),"UTF-8");

			return reader;
		} catch (ProtocolException e) {
			e.printStackTrace();
			throw new Exception("Exception raised on simple request. " ,e);
		}
	}
	
	
}
