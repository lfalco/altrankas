package cat.lluisfalco.altran.kas.model;

public class SearchMeta {
	
	String sort;
	
	int count;
	
	Result[] results;

	public String getSort() {
		return sort;
	}

	public int getCount() {
		return count;
	}

	public Result[] getResults() {
		return results;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public void setResults(Result[] results) {
		this.results = results;
	}
	

	
	
	
}
