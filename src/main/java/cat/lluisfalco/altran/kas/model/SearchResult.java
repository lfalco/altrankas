package cat.lluisfalco.altran.kas.model;

public class SearchResult {
	

	String help;
	
	String success;
	
	SearchMeta result;

	public String getHelp() {
		return help;
	}

	public String getSuccess() {
		return success;
	}

	public SearchMeta getResult() {
		return result;
	}

	public void setHelp(String help) {
		this.help = help;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public void setResult(SearchMeta result) {
		this.result = result;
	}
	
	
}
