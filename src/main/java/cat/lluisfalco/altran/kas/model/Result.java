package cat.lluisfalco.altran.kas.model;

public class Result {
	
	
	String code;
	
	Language url_tornada;
	
	Organization organization;

	public String getCode() {
		return code;
	}

	public Language getUrl_tornada() {
		return url_tornada;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setUrl_tornada(Language url_tornada) {
		this.url_tornada = url_tornada;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
	
	/** -- Bla bla ... -- */
	
	
	
	
}
