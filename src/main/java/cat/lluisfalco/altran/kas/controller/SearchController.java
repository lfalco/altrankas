package cat.lluisfalco.altran.kas.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cat.lluisfalco.altran.kas.service.SearchService;
import cat.lluisfalco.altran.kas.service.dto.ResponseDTO;


@RestController
public class SearchController {
	//final static Logger log = Logger.getLogger(SearchController.class);
	
	@Autowired
	SearchService searchService;

	public SearchController() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	@RequestMapping(value = "/package_search", method = RequestMethod.GET) 
	public @ResponseBody ArrayList<ResponseDTO> getAllCodeAndDescriptionAndUrl(@RequestParam("language") String language){
		return searchService.getAllCodeAndDescriptionAndUrl(language);
	}

	
}
